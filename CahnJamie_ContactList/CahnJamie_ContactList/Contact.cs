﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CahnJamie_ContactList
{
    public class Contact
    {
        private int _id;
        private string _firstName;
        private string _lastName;
        private int _phoneNumber;
        private string _email;
        private string _relation;
        private int _imageRelationPic;

        public Contact(int id, string firstName, string lastName, int phoneNumber, string email, string relation, int imagerelationpic)
        {
            _id = id;
            _firstName = firstName;
            _lastName = lastName;
            _phoneNumber = phoneNumber;
            _email = email;
            _relation = relation;
            _imageRelationPic = imagerelationpic;

        }

        public int Id { get => _id; set => _id = value; }
        public string FirstName { get => _firstName; set => _firstName = value; }
        public string LastName { get => _lastName; set => _lastName = value; }
        public int PhoneNumber { get => _phoneNumber; set => _phoneNumber = value; }
        public string Email { get => _email; set => _email = value; }
        public string Relation { get => _relation; set => _relation = value; }
        public int ImageRelationPic { get => _imageRelationPic; set => _imageRelationPic = value; }

        public override string ToString()
        {
            return LastName + " " + FirstName;
        }
    }
}
