﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace CahnJamie_ContactList
{
    public partial class AddContact : Form
    {
        bool connStatus = false; //connection status variable

        MySqlConnection conn = new MySqlConnection(); //connection variable

        int currentRow = 0; //count the current row 

        DataTable theData = new DataTable(); //create a new datatable

        private string BuildConnectionString()
        {
            string serverIP = "";

            try
            {
                //open text file via streamreader
                using (StreamReader sr = new StreamReader(@"C:\VFW\connect.txt"))
                {
                    //read the ip from file
                    serverIP = sr.ReadLine();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            //return the connection string
            return "server=" + serverIP + ";userid=dbsAdmin;password=password;database=ContactList;port=8889;SslMode=none";
        } //build connection string method

        private void Connect(string myConnectionString)
        {
            try
            {
                conn.ConnectionString = myConnectionString;
                conn.Open();
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        {
                            MessageBox.Show("Can't Resolve Host Address.\n" + myConnectionString);
                        }
                        break;
                    case 1045:
                        {
                            MessageBox.Show("Invalid Username/Password");
                        }
                        break;
                    default:
                        {
                            MessageBox.Show(e.ToString() + "\n" + myConnectionString);
                        }
                        break;
                }
            }
        } //connect method

        public AddContact()
        {
            InitializeComponent();
            cmbRelation.SelectedIndex = 16;
        } //if contact is added

        private void NewForm_UpdateContact(object sender, EventArgs e)
        {
            txtFirstName.Text = ((Contact)sender).FirstName;
            txtLastName.Text = ((Contact)sender).LastName;
            txtPhoneNumber.Text = ((Contact)sender).PhoneNumber.ToString();
            txtEmailAddress.Text = ((Contact)sender).Email;
            cmbRelation.Text = ((Contact)sender).Relation;
            imageRelationPic = ((Contact)sender).ImageRelationPic;
            pbRelation.Image = imageList1.Images[imageRelationPic];

            updatedContact = new Contact(((Contact)sender).Id, ((Contact)sender).FirstName, ((Contact)sender).LastName, ((Contact)sender).PhoneNumber, ((Contact)sender).Email, ((Contact)sender).Relation, ((Contact)sender).ImageRelationPic);
            
        }

        public AddContact(bool x)
        {
            Form1 newForm = null; //create a version of form1 so that I can call an eventhandler from it
   
            //update form to instead of being an "Add Contact" Form it will be an "Update Contact" Form
            newForm.updateContact += NewForm_UpdateContact;
            btnClear.Text = "Reset"; //Change the button of Clear to "Reset"
            btnAddContact.Text = "Update Contact"; //change the button of "AddContact" to "UpdateContact"
            InitializeComponent();
            

        } //if contact is updated

        //variables
        Contact updatedContact;
        string firstName;
        string lastName;
        string phoneNumberString;
        string emailAddress;
        string relation;
        int phoneNumber;
        int id;
        int imageRelationPic;
        Random rand = new Random();
        public EventHandler sendContactList;

        List<Contact> contactList = new List<Contact>();

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (btnClear.Text == "Clear")
            {
                //clear fields
                txtFirstName.Clear();
                txtLastName.Clear();
                txtPhoneNumber.Clear();
                txtEmailAddress.Clear();
                cmbRelation.SelectedItem = "Other";
                pbRelation.Image = imageList1.Images[17];
            }
            else
            {
                txtFirstName.Text = updatedContact.FirstName;
                txtLastName.Text = updatedContact.LastName;
                txtPhoneNumber.Text = updatedContact.PhoneNumber.ToString();
                txtEmailAddress.Text = updatedContact.Email;
                cmbRelation.Text = updatedContact.Relation;
                pbRelation.Image = imageList1.Images[updatedContact.ImageRelationPic];

            }
        } //clear fields

        private bool UpdateAContact(Contact tmp)
        {
            try
            {
                //build connection string
                string connStringUpdate = BuildConnectionString();

                //build sql query
                string sql = $"UPDATE MyContacts SET id={tmp.Id},FirstName={tmp.FirstName},LastName={tmp.LastName},PhoneNumber={tmp.PhoneNumber},Email={tmp.Email},Relation={tmp.Relation}";

                //create new connection
                MySqlConnection conn2 = new MySqlConnection(connStringUpdate);

                //AddUser MySqlCommand
                MySqlCommand AddUser = new MySqlCommand(sql, conn2);

                //MySql Reader
                MySqlDataReader newReader;

                conn2.Open();
                newReader = AddUser.ExecuteReader();
                MessageBox.Show("Contact Updated!");

                while (newReader.Read())
                {

                }
                //close connection
                conn2.Close();
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        {
                            MessageBox.Show("Can't Resolve Host Address.\n");
                        }
                        break;
                    case 1045:
                        {
                            MessageBox.Show("Invalid Username/Password.");
                        }
                        break;
                    default:
                        {
                            MessageBox.Show(e.ToString());
                        }
                        break;
                }
            }
            return false;
        } //update a contact

        private bool AddAContact(Contact tmp)
        {
            try
            {
                //build connection string
                string connStringAdd = BuildConnectionString();

                //build sql query
                //string sql = $"ADD MyContacts SET id={tmp.Id},FirstName={tmp.FirstName},LastName={tmp.LastName},PhoneNumber={tmp.PhoneNumber},Email={tmp.Email},Relation={tmp.Relation}";

                string sql = $"INSERT INTO MyContacts (id, FirstName, LastName, PhoneNumber, Email, Relation) VALUES ('{tmp.Id}','{tmp.FirstName}', '{tmp.LastName}', '{tmp.PhoneNumber}', '{tmp.Email}', '{tmp.Relation}')";
                //create new connection
                MySqlConnection conn2 = new MySqlConnection(connStringAdd);

                //AddUser MySqlCommand
                MySqlCommand AddUser = new MySqlCommand(sql, conn2);

                //MySql Reader
                MySqlDataReader newReader;

                conn2.Open();
                newReader = AddUser.ExecuteReader();
                MessageBox.Show("Contact Added!");

                while (newReader.Read())
                {

                }
                //close connection
                conn2.Close();
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        {
                            MessageBox.Show("Can't Resolve Host Address.\n");
                        }
                        break;
                    case 1045:
                        {
                            MessageBox.Show("Invalid Username/Password.");
                        }
                        break;
                    default:
                        {
                            MessageBox.Show(e.ToString());
                        }
                        break;
                }
            }
            return false;
        } //add a contact

        private void btnAddContact_Click(object sender, EventArgs e)
        {
            if (btnAddContact.Text == "Add Contact")
            {   
                firstName = txtFirstName.Text;
                lastName = txtLastName.Text;
                phoneNumberString = txtPhoneNumber.Text;
                emailAddress = txtEmailAddress.Text;
                relation = cmbRelation.Text;
                int.TryParse(phoneNumberString, out phoneNumber);

                if (cmbRelation.Text == "Parent")
                {
                    imageRelationPic = 0;
                }
                else if (cmbRelation.Text == "Mother")
                {
                    imageRelationPic = 1;
                }
                else if (cmbRelation.Text == "Father")
                {
                    imageRelationPic = 2;
                }
                else if (cmbRelation.Text == "Brother")
                {
                    imageRelationPic = 3;
                }
                else if (cmbRelation.Text == "Sister")
                {
                    imageRelationPic = 4;
                }
                else if (cmbRelation.Text == "Spouse")
                {
                    imageRelationPic = 5;
                }
                else if (cmbRelation.Text == "Child")
                {
                    imageRelationPic = 6;
                }
                else if (cmbRelation.Text == "Friend")
                {
                    imageRelationPic = 7;
                }
                else if (cmbRelation.Text == "Relative")
                {
                    imageRelationPic = 8;
                }
                else if (cmbRelation.Text == "Domestic Partner")
                {
                    imageRelationPic = 9;
                }
                else if (cmbRelation.Text == "Partner")
                {
                    imageRelationPic = 10;
                }
                else if (cmbRelation.Text == "Manager")
                {
                    imageRelationPic = 11;
                }
                else if (cmbRelation.Text == "Assistant")
                {
                    imageRelationPic = 12;
                }
                else if (cmbRelation.Text == "Reference")
                {
                    imageRelationPic = 13;
                }
                else if (cmbRelation.Text == "Co-Worker")
                {
                    imageRelationPic = 14;
                }
                else if (cmbRelation.Text == "Employee")
                {
                    imageRelationPic = 15;
                }
                else if (cmbRelation.Text == "Other")
                {
                    imageRelationPic = 16;
                }
                else
                {
                    imageRelationPic = 17;
                }


                id = rand.Next(1, 999999999); //develop better scenario to make this work (need a non-repeated number)

                Contact tmp = new Contact(id, firstName, lastName, phoneNumber, emailAddress, relation, imageRelationPic);

                contactList.Add(tmp); //add tmp contact to list
                try
                {
                    conn.Open();
                    AddAContact(tmp);
                    conn.Close();
                }
                catch (MySqlException)
                {

                }
                if (sendContactList != null) //created null check
                {
                    sendContactList(contactList, e);
                }

                this.Close();
            } //add a contact
            else
            {
                txtFirstName.Text = firstName;
                txtLastName.Text = lastName;
                txtPhoneNumber.Text = phoneNumberString;
                txtEmailAddress.Text = emailAddress;
                cmbRelation.Text = relation;
                int.TryParse(phoneNumberString, out phoneNumber);

                if (cmbRelation.Text == "Parent")
                {
                    imageRelationPic = 0;
                }
                else if (cmbRelation.Text == "Mother")
                {
                    imageRelationPic = 1;
                }
                else if (cmbRelation.Text == "Father")
                {
                    imageRelationPic = 2;
                }
                else if (cmbRelation.Text == "Brother")
                {
                    imageRelationPic = 3;
                }
                else if (cmbRelation.Text == "Sister")
                {
                    imageRelationPic = 4;
                }
                else if (cmbRelation.Text == "Spouse")
                {
                    imageRelationPic = 5;
                }
                else if (cmbRelation.Text == "Child")
                {
                    imageRelationPic = 6;
                }
                else if (cmbRelation.Text == "Friend")
                {
                    imageRelationPic = 7;
                }
                else if (cmbRelation.Text == "Relative")
                {
                    imageRelationPic = 8;
                }
                else if (cmbRelation.Text == "Domestic Partner")
                {
                    imageRelationPic = 9;
                }
                else if (cmbRelation.Text == "Partner")
                {
                    imageRelationPic = 10;
                }
                else if (cmbRelation.Text == "Manager")
                {
                    imageRelationPic = 11;
                }
                else if (cmbRelation.Text == "Assistant")
                {
                    imageRelationPic = 12;
                }
                else if (cmbRelation.Text == "Reference")
                {
                    imageRelationPic = 13;
                }
                else if (cmbRelation.Text == "Co-Worker")
                {
                    imageRelationPic = 14;
                }
                else if (cmbRelation.Text == "Employee")
                {
                    imageRelationPic = 15;
                }
                else if (cmbRelation.Text == "Other")
                {
                    imageRelationPic = 16;
                }
                else
                {
                    imageRelationPic = 17;
                }


                id = rand.Next(1, 999999999); //develop better scenario to make this work (need a non-repeated number)

                Contact tmp = new Contact(id, firstName, lastName, phoneNumber, emailAddress, relation, imageRelationPic);

                contactList.Add(tmp); //add tmp contact to list

                conn.Open();
                UpdateAContact(tmp);
                conn.Close();
                sendContactList(contactList, e);
                this.Close();
            }
        } //add new contact

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        } //lets user go back without saving information
    }
}
/*
 * UPDATES to AddContact:
 * - created a null checker so that data is always returned and there is never null data returned
 * - fixed issue not allowing program to create new
 * - TODO: fix issue preventing user from updating user
 * - TODO: fix issue preventing user adding/updating from hitting the database
 * - 
 */