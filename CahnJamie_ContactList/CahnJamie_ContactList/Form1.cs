﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace CahnJamie_ContactList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            try
            {
                //invoke string build
                string connString = BuildConnectionString();

                //invoke connect
                Connect(connString);
                connStatus = true;

                //invoke data retrieval
                RetrieveData();

                //close connString
                connStatus = false;
                conn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        } //initialize form1

        bool connStatus = false; //connection status variable

        MySqlConnection conn = new MySqlConnection(); //connection variable

        int currentRow = 0; //count the current row 

        DataTable theData = new DataTable(); //create a new datatable

        private string BuildConnectionString()
        {
            string serverIP = "";

            try
            {
                //open text file via streamreader
                using (StreamReader sr = new StreamReader(@"C:\VFW\connect.txt"))
                {
                    //read the ip from file
                    serverIP = sr.ReadLine();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            //return the connection string
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=ContactList;port=8889;SslMode=none";
        } //build connection string method

        private void Connect(string myConnectionString)
        {
            try
            {
                conn.ConnectionString = myConnectionString;
                conn.Open();
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        {
                            MessageBox.Show("Can't Resolve Host Address.\n" + myConnectionString);
                        }
                        break;
                    case 1045:
                        {
                            MessageBox.Show("Invalid Username/Password");
                        }
                        break;
                    default:
                        {
                            MessageBox.Show(e.ToString() + "\n" + myConnectionString);
                        }
                        break;
                }
            }
        } //connect method
        List<Contact> contactList = new List<Contact>();

        private bool RetrieveData()
        {
            //create sql statement
            string sql = "SELECT id, FirstName, LastName, PhoneNumber, Email, Relation FROM MyContacts LIMIT 15";

            //create the data adapter
            MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);

            //the fill method adds rows to match the data source
            adr.Fill(theData);

            //check number of records
            int numOfRecords = theData.Select().Length;

            //populate List
            for (int i = 0; i < theData.Rows.Count; i++)
            {
                string _idString = theData.Rows[i]["id"].ToString();
                int _id;
                int.TryParse(_idString, out _id);
                string _firstName = theData.Rows[i]["FirstName"].ToString();
                string _lastName = theData.Rows[i]["LastName"].ToString();
                string _phoneNumberString = theData.Rows[i]["PhoneNumber"].ToString();
                int _phoneNumber;
                int.TryParse(_phoneNumberString, out _phoneNumber);
                string _email = theData.Rows[i]["Email"].ToString();
                string _relation = theData.Rows[i]["Relation"].ToString();

                //the following grabs the relation and finds the picture associated to it
                int _ImageRelationPic;
                if (_relation == "Parent")
                {
                    _ImageRelationPic = 1;
                }
                else if (_relation == "Mother")
                {
                    _ImageRelationPic = 2;
                }
                else if (_relation == "Father")
                {
                    _ImageRelationPic = 3;
                }
                else if (_relation == "Brother")
                {
                    _ImageRelationPic = 4;
                }
                else if (_relation == "Sister")
                {
                    _ImageRelationPic = 5;
                }
                else if (_relation == "Spouse")
                {
                    _ImageRelationPic = 6;
                }
                else if (_relation == "Child")
                {
                    _ImageRelationPic = 7;
                }
                else if (_relation == "Friend")
                {
                    _ImageRelationPic = 8;
                }
                else if (_relation == "Relative")
                {
                    _ImageRelationPic = 9;
                }
                else if (_relation == "Domestic Partner")
                {
                    _ImageRelationPic = 10;
                }
                else if (_relation == "Partner")
                {
                    _ImageRelationPic = 11;
                }
                else if (_relation == "Manager")
                {
                    _ImageRelationPic = 12;
                }
                else if (_relation == "Assistant")
                {
                    _ImageRelationPic = 13;
                }
                else if (_relation == "Reference")
                {
                    _ImageRelationPic = 14;
                }
                else if (_relation == "Co-Worker")
                {
                    _ImageRelationPic = 15;
                }
                else if (_relation == "Employee")
                {
                    _ImageRelationPic = 16;
                }
                else if (_relation == "Other")
                {
                    _ImageRelationPic = 17;
                }
                else
                {
                    _ImageRelationPic = 18;
                }

                //add the "tmp" contact to the contastList
                Contact tmp = new Contact(_id, _firstName, _lastName, _phoneNumber, _email, _relation, _ImageRelationPic);
                contactList.Add(tmp);
            }

            return true;
        } //retrieve data method

        public EventHandler updateContact;

        private void OpenContactForm_SendContactList(object sender, EventArgs e)
        {
            for (int i = 0; i < ((List<Contact>)sender).Count; i++)
            {
                contactList.Add(((List<Contact>)sender)[i]);
                lbContacts.Items.Add(contactList[i].ToString());
            }
        }

        private void btnAddContact_Click(object sender, EventArgs e)
        {
            AddContact openContactForm = new AddContact();
            openContactForm.sendContactList += OpenContactForm_SendContactList;
            openContactForm.ShowDialog();
            openContactForm.Close();
        }

        private void btnUpdateContact_Click(object sender, EventArgs e)
        {
            if (contactList.Count == 0)
            {
                MessageBox.Show("Please create a contact.");
            }
            else
            {
                if (lbContacts.SelectedItems[0] != null) //TODO: fic this
                {
                    Contact tmp = null;
                    for (int i = 0; i < contactList.Count; i++)
                    {
                        if (lbContacts.SelectedItems[0].ToString() == contactList[i].ToString())
                        {
                            tmp = contactList[i];
                        }
                    }

                    bool x = true;
                    AddContact openContactForm = new AddContact(x);
                    openContactForm.ShowDialog();
                    updateContact(tmp, e);


                    openContactForm.Close();
                }
                else
                {
                    MessageBox.Show("No Contact Selected!");
                }
            }
        }
        private bool DeleteAContact(Contact tmp)
        {
            try
            {
                //build connection string
                string connStringDelete = BuildConnectionString();

                //build sql query
                string sql = $"DELETE FROM MyContacts WHERE id={tmp.Id},FirstName={tmp.FirstName},LastName={tmp.LastName},PhoneNumber={tmp.PhoneNumber},Email={tmp.Email},Relation={tmp.Relation}";

                //create new connection
                MySqlConnection conn2 = new MySqlConnection(connStringDelete);

                //AddUser MySqlCommand
                MySqlCommand AddUser = new MySqlCommand(sql, conn2);

                //MySql Reader
                MySqlDataReader newReader;

                conn2.Open();
                newReader = AddUser.ExecuteReader();
                MessageBox.Show("Contact Deleted!");

                while (newReader.Read())
                {

                }
                //close connection
                conn2.Close();
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        {
                            MessageBox.Show("Can't Resolve Host Address.\n");
                        }
                        break;
                    case 1045:
                        {
                            MessageBox.Show("Invalid Username/Password.");
                        }
                        break;
                    default:
                        {
                            MessageBox.Show(e.ToString());
                        }
                        break;
                }
            }
            return false;
        } //delete a contact

        private void btnRemoveContact_Click(object sender, EventArgs e)
        {
            if (contactList.Count == 0)
            {
                MessageBox.Show("Please create a contact first.");
            }
            else
            {
                if (lbContacts.SelectedItems[0] != null)
                {
                    //this block of code is an update from the original program
                    Contact tmp = null;
                    for (int i = 0; i < contactList.Count; i++)
                    {
                        if (lbContacts.SelectedItems[0].ToString() == contactList[i].ToString())
                        {
                            tmp = contactList[i];
                        }
                    }
                    DeleteAContact(tmp); //delete the contact from the database

                    //delete the contact from the list
                    for (int i = 0; i < contactList.Count; i++)
                    {
                        if (lbContacts.SelectedItems[0].ToString() == contactList[i].ToString())
                        {
                            contactList.RemoveAt(i);
                        }
                    }

                    lbContacts.Items.Clear(); //clear the listbox

                    //repopulate the listbox
                    for (int i = 0; i < contactList.Count; i++)
                    {
                        lbContacts.Items.Add(contactList[i].ToString());
                    }
                }
            }
        }
    }
}
/*
 * UPDATES to Form1:
 * - updated remove contact issue
 * - updated update contact issue
 * - updated the way to delete and the way to update a contact
 * - 
 */
